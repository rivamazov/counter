#Download base image ubuntu 20.04
FROM ubuntu:20.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update

# Install Phoenix
RUN apt install -y wget gnupg
RUN wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb && dpkg -i erlang-solutions_2.0_all.deb 
RUN apt update && \
	apt install -y esl-erlang && \
	apt install -y elixir && \
	# for browser reload
	apt install -y inotify-tools && \
	rm -rf /var/lib/apt/lists/* && \
	apt clean

# Set Enviornmental Variables
ENV LANG: C.UTF-8
ENV ELIXIR_VERSION: 1.13.4
ENV MIX_ENV: prod
ENV PORT: 4000

# Pre Build
RUN mix local.hex --force && mix local.rebar --force && mix hex.info


# copy start script and define default command for the container

ENV APP_HOME /app
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

ENTRYPOINT ["scripts/build.sh"]
EXPOSE 4000





