#!/bin/bash
mix deps.get
mix compile

# Compile assets
mix assets.deploy

# Custom tasks (like DB migrations)

# Finally run the server
mix phx.server

